﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/5/2015 1:32:23 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RS.MvcIoc.WebApp.Models
{
    public class DebugMessageService: IDebugMessageService
    {
        public string Message
        {
            get
            {
                return DateTime.Now.ToString();
            }
           
        }
    }
}