﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RS.MvcIoc.WebApp.Models
{
    public interface IAnalyticService
    {
        string Code { get; }
    }
}
