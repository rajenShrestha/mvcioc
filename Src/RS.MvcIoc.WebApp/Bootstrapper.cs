using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc3;
using RS.MvcIoc.WebApp.Models;

namespace RS.MvcIoc.WebApp
{
    public static class Bootstrapper
    {
        public static void Initialise()
        {
            var container = BuildUnityContainer();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();            

            RegisterTypes(container);
            return container;
        }

        private static void RegisterTypes(UnityContainer container)
        {
            container.RegisterType<IProteinTrackingService, ProteinTrackingService>();
            container.RegisterType<IDebugMessageService, DebugMessageService>();

            //container.RegisterType<IProteinRepository, ProteinRepository>();
            container.RegisterInstance<IProteinRepository>(new ProteinRepository(30));
            
            //if you used below,then register service as well
            //container.RegisterType<IProteinRepository, ProteinRepository>("DetaultProteinRepository", new InjectionConstructor(30));
            //container.RegisterType<IProteinTrackingService,ProteinTrackingService>(new InjectionConstructor(container.Resolve<IProteinRepository>("DetaultProteinRepository")));



            container.RegisterType<IAnalyticService, AnalyticService>();
        }
    }
}