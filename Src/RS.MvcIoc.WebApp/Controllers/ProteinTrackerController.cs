﻿using RS.MvcIoc.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RS.MvcIoc.WebApp.Controllers
{
    public class ProteinTrackerController : Controller
    {
        private IProteinTrackingService _proteinTrackingService;

        public ProteinTrackerController(IProteinTrackingService proteinTrackingService)
        {
            _proteinTrackingService = proteinTrackingService;
        }

        public ActionResult Index()
        {
            ViewBag.Total = _proteinTrackingService.Total;
            ViewBag.Goal = _proteinTrackingService.Goal;

            return View();
        }

        [HttpGet]
        public ActionResult Add()
        {
            return View(_proteinTrackingService);
        }

        [HttpPost]
        public ActionResult Add(int total)
        {
            _proteinTrackingService.AddProtein(total);

            ViewBag.Total = _proteinTrackingService.Total;
            ViewBag.Goal = _proteinTrackingService.Goal;

            return View(_proteinTrackingService);
        }     
    }
}
