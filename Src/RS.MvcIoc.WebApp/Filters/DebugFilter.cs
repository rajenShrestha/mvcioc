﻿using Microsoft.Practices.Unity;
using RS.MvcIoc.WebApp.Models;
/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/5/2015 1:28:49 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RS.MvcIoc.WebApp.Filters
{
    /// <summary>
    /// injection is filter demo
    /// </summary>
    public class DebugFilter: ActionFilterAttribute
    {
        private IDebugMessageService _debugMessageService;

        public DebugFilter(IDebugMessageService debugMessageService)
        {
            _debugMessageService = debugMessageService;
        }

        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Write(_debugMessageService.Message);
        }
 
    }
}