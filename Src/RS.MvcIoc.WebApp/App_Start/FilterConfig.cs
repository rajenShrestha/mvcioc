﻿using RS.MvcIoc.WebApp.Filters;
using System.Web;
using System.Web.Mvc;

namespace RS.MvcIoc.WebApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(DependencyResolver.Current.GetService<DebugFilter>());
        }
    }
}