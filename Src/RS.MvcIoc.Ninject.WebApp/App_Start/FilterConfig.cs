﻿using RS.MvcIoc.NinjectDi.WebApp.Filters;
using System.Web;
using System.Web.Mvc;

namespace RS.MvcIoc.NinjectDi.WebApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //Note: Since the IDebugMessageService is mapped to DebugMessageService on NijectWebCommon, we do no have to worry about DebugFilter
            filters.Add(DependencyResolver.Current.GetService<DebugFilter>());
        }
    }
}