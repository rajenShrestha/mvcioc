﻿using System;
namespace RS.MvcIoc.NinjectDi.WebApp.Models
{
    public interface IProteinTrackingService
    {
        void AddProtein(int amount);
        int Goal { get; set; }
        int Total { get; set; }
    }
}
