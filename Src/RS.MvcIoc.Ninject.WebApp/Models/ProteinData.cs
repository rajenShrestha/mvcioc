﻿namespace RS.MvcIoc.NinjectDi.WebApp.Models
{
    public class ProteinData
    {
        public int Total { get; set; }
        public int Goal { get; set; }
    }
}