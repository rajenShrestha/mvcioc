﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RS.MvcIoc.NinjectDi.WebApp.Models
{
    public interface IDebugMessageService
    {
        string Message { get;  }
    }
}
