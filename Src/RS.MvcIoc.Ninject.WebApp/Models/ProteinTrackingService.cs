﻿/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/4/2015 6:28:44 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RS.MvcIoc.NinjectDi.WebApp.Models
{
    public class ProteinTrackingService : IProteinTrackingService
    {
        private IProteinRepository _repository;

        public ProteinTrackingService(IProteinRepository repository)
        {
            _repository = repository;
        }

        public int Total
        {
            get { return _repository.GetData(new DateTime().Date).Total; }
            set { _repository.SetTotal(new DateTime().Date, value); }
        }

        public int Goal
        {
            get { return _repository.GetData(new DateTime().Date).Goal; }
            set { _repository.SetGoal(new DateTime().Date, value); }
        }

        public void AddProtein(int amount)
        {
            Total += amount;
        }
    }
}