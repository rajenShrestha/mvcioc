﻿using Ninject;
using RS.MvcIoc.NinjectDi.WebApp.Models;
/**
* Copyright 2015 rshrestha
* All right are reserved. Reproduction or transmission in whole or in 
* part, in any form or by any means, electronic, mechanical or otherwise
* is published without the prior written consent of the copyright owner.
 * 
 * Author:rshrestha
 * Time: 4/5/2015 2:26:46 PM
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RS.MvcIoc.NinjectDi.WebApp.ViewPages
{
    //viewpageactivator

    /// <summary>
    /// view injection demo
    /// </summary>
    public class ProteinTrackerBasePage : WebViewPage
    {
        [Inject]
        public IAnalyticService AnalyticService { get; set; }

        public override void Execute()
        {
            
        }
    }
}